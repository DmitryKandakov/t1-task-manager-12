package ru.t1.dkandakov.tm.api;

public interface ICommandController {

    void showSystemInfo();

    void showExit();

    void showErrorArgument();

    void showErrorCommand();

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

}