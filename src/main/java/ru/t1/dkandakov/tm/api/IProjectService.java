package ru.t1.dkandakov.tm.api;

import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    void remove(Project project);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}